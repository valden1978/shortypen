/*
 * u8g2_hw.c
 *
 *  Created on: Jul 15, 2022
 *      Author: jiri.dobry
 */

#include "u8g2_hw.h"

uint8_t u8x8_stm32_gpio_and_delay_cb(U8X8_UNUSED u8x8_t *u8x8, U8X8_UNUSED uint8_t msg, U8X8_UNUSED uint8_t arg_int, U8X8_UNUSED void *arg_ptr)
{
  switch(msg)
  {
    case U8X8_MSG_DELAY_MILLI:
      HAL_Delay(arg_int);
     break;
    default:
      break;
  }
  return 1;
}

uint8_t u8x8_byte_hw_i2c(u8x8_t *u8x8, uint8_t msg, uint8_t arg_int, void *arg_ptr)
{
  static uint8_t bufferIndex;
  static uint8_t buffer[DATA_BUFFER_SIZE+1];

  switch(msg)
  {
    case U8X8_MSG_BYTE_SEND:          //collect
      {
        uint8_t *p = arg_ptr;
        for (int i=1; i <= arg_int; i++)
        {
          buffer[bufferIndex] = *(p++);
          bufferIndex++;
        }
      }
      break;
//    case U8X8_MSG_BYTE_INIT:
//      break;
//    case U8X8_MSG_BYTE_SET_DC:
//      break;
    case U8X8_MSG_BYTE_START_TRANSFER:
      bufferIndex = 0;
      break;
    case U8X8_MSG_BYTE_END_TRANSFER:
       HAL_I2C_Master_Transmit(&SSD1306_I2C_BUS, SSD1306_ADDRESS, (uint8_t *)buffer, bufferIndex, I2C_TIMEOUT);
      break;
    default:
      return 0;
  }
  return 1;
}
