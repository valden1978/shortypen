/*
 * oled.h
 *
 *  Created on: Jul 16, 2022
 *      Author: jiri.dobry
 */

#ifndef OLED_H_
#define OLED_H_

#include <stdint.h>

#define SYMBOL_Batt_image_width  24
#define SYMBOL_Batt_image_height 10
extern const uint8_t symbol_Batt_image_bits[];

#define SYMBOL_V_image_width  8
#define SYMBOL_V_image_height 10
extern const uint8_t symbol_V_image_bits[];

#define SYMBOL_A_image_width  8
#define SYMBOL_A_image_height 10
extern const uint8_t symbol_A_image_bits[];

#define SYMBOL_omega_image_width  8
#define SYMBOL_omega_image_height 8
extern const uint8_t symbol_omega_image_bits[];

#define SYMBOL_more_image_width  8
#define SYMBOL_more_image_height 12
extern const uint8_t symbol_more_image_bits[];

#define SYMBOL_speakerA_image_width  8
#define SYMBOL_speakerA_image_height 14
extern const uint8_t symbol_speakerA_image_bits[];

#define SYMBOL_speakerB_image_width  8
#define SYMBOL_speakerB_image_height 14
extern const uint8_t symbol_speakerB_image_bits[];

#define SYMBOL_Cal_image_width  18
#define SYMBOL_Cal_image_height 10
extern const uint8_t symbol_Cal_image_bits[];

#define SYMBOL_BattBar_image_width  10
#define SYMBOL_BattBar_image_height 24
extern const uint8_t symbol_BattBar_image_bits[];

#define SYMBOL_Off_image_width  16
#define SYMBOL_Off_image_height 9
extern const uint8_t symbol_Off_image_bits[];

#endif /* OLED_H_ */
