#include "oled.h"

/*
 * to edit see https://xbm.jazzychad.net/
 */

const uint8_t symbol_Batt_image_bits[] = {
  0x1f,0x00,0x00,0x3f,0x80,0x61,0x33,0x80,0x61,0x33,0xc0,0xf3,
  0x1f,0x8f,0x61,0x9f,0x9f,0x61,0xb3,0x99,0x61,0xb3,0x99,0x61,
  0xbf,0xbf,0xe3,0x1f,0x3f,0xc3};

const uint8_t symbol_V_image_bits[] = {
  0xc3,0xc3,0xc3,0x66,0x66,0x66,0x3c,0x3c,0x18,0x18};

const uint8_t symbol_A_image_bits[] = {
  0x18,0x18,0x3c,0x3c,0x66,0x66,0x7e,0xff,0xc3,0xc3};

const uint8_t symbol_omega_image_bits[] = {
  0x3c,0x7e,0xe7,0xc3,0xc3,0x66,0xe7,0xe7};

const uint8_t symbol_more_image_bits[] = {
  0x07,0x0e,0x1c,0x38,0x70,0xe0,0xe0,0x70,0x38,0x1c,0x0e,0x07};

const uint8_t symbol_speakerA_image_bits[] = {
  0xc0,0xe0,0xf0,0xdf,0xcd,0xcd,0xcd,0xcd,0xcd,0xcd,0xdf,0xf0,
  0xe0,0xc0};

const uint8_t symbol_speakerB_image_bits[] = {
  0x0c,0x18,0x32,0x66,0x6c,0xc9,0xda,0xda,0xc9,0x6c,0x66,0x32,
  0x18,0x0c};

const uint8_t symbol_Cal_image_bits[] = {
  0x1e,0x00,0xfc,0x3f,0x80,0xfd,0x33,0x80,0xfd,0x33,0x80,0xfd,
  0x03,0x8f,0xfd,0x83,0x9f,0xfd,0xb3,0x99,0xfd,0xb3,0x99,0xfd,
  0xbf,0xbf,0xff,0x1e,0x3f,0xff};

const uint8_t symbol_BattBar_image_bits[] = {
  0x78,0xfc,0x48,0xfc,0x48,0xfc,0xff,0xff,0x01,0xfe,0x01,0xfe,
  0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,
  0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,
  0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,0x01,0xfe,0xff,0xff};

const uint8_t symbol_Off_image_bits[] = {
  0x1e,0x00,0x3f,0xc6,0x33,0xe7,0x33,0x63,0xb3,0xf7,0xb3,0xf7,
  0x33,0x63,0x3f,0x63,0x1e,0x63};


#if 0
const uint8_t symbol_BattBarDots_image_bits[] = {
  0x28,0xfc,0x40,0xfc,0x08,0xfc,0x55,0xfd,0x00,0xfe,0x01,0xfc,
  0x00,0xfe,0x01,0xfc,0x00,0xfe,0x01,0xfc,0x00,0xfe,0x01,0xfc,
  0x00,0xfe,0x01,0xfc,0x00,0xfe,0x01,0xfc,0x00,0xfe,0x01,0xfc,
  0x00,0xfe,0x01,0xfc,0x00,0xfe,0x01,0xfc,0xaa,0xfe};

#define image_width  12
#define image_height 10
const uint8_t symbol_flash_image_bits[] = {
  0xc0,0xf0,0x60,0xf0,0x30,0xf0,0x18,0xf0,0xec,0xf1,0xb8,0xf1,
  0xc0,0xf0,0x68,0xf0,0x38,0xf0,0x78,0xf0};

#define image_width  12
#define image_height 10
const uint8_t symbol_snail_image_bits[] = {
  0x00,0xf0,0x00,0xf0,0x00,0xf0,0x00,0xf3,0xa1,0xf7,0xd2,0xff,
  0xcc,0xff,0xfc,0xff,0xfc,0xf7,0x00,0xf0};
#endif
