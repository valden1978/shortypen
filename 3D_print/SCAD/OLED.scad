$fn=32;

difference()
{
    cube([38,13,3.5]);
    
    hull()
    {
      translate([11.938,13-2.429,-1]) cylinder (d=4,h=10);
      translate([11.938,13-2.429+5,-1]) cylinder (d=4,h=10);
    }
    hull()
    {
        translate([22.479,13-12.589,-1]) cylinder (d=4,h=10);
        translate([22.479,13-(12.589+5),-1]) cylinder (d=4,h=10);
    }
    translate([(15-12)/2,0.5,2.5]) cube([40,13,5]);
    translate([(15-12)/2+0.7+10,0.5+0.7-5,0.8]) cube([10,12-2*0.7+5,5]);
    translate([(15-12)/2+0.7+5,0.5+0.7,0.8]) cube([25,12-2*0.7+5,5]);
    translate([(15-12)/2+0.7+5+27,0.5+0.7,-.1]) cube([25,12-2*0.7,5]);
}