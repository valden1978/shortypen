/*
 * shortyPen.c
 *
 *  Created on: Jul 18, 2022
 *      Author: jiri.dobry
 */


#include "main.h"
#include "u8g2.h"
#include "u8g2_hw.h"
#include "eeprom_emul.h"
#include "adc.h"
#include "oled.h"
#include <stdio.h>
#include <math.h>
#include <stdbool.h>

#define SW_VERSION "1.02.02"

#define ARR_400 (__LL_TIM_CALC_ARR(16000000, 0, 400))
#define ARR_1760 (__LL_TIM_CALC_ARR(16000000, 0, 1760)) // note "A" octave 6
#define ARR_2000 (__LL_TIM_CALC_ARR(16000000, 0, 2000))

#define INACTIVE_POWER_OFF_TIME 300000
//#define INACTIVE_POWER_OFF_TIME 10000 /* just for tests */
#define INACTIVE_POWER_OFF_TIME_BEEPER 3000


#define BUTTON_SHORT_TIME 100
#define BUTTON_LONG_TIME 1000

#define BUTTON1_I 0
#define BUTTON2_I 1

#define QUADRATIC_ESTIMATION 1

#define EE_INDEX_INT_ZERO 1
#define EE_INDEX_EXT_ZERO 3
#define EE_INDEX_MODE 5
#define EE_INDEX_FLIPMODE 6
#define EE_INDEX_BEEPER 7

#define EE_INDEX_INT_Q_A 2
#define EE_INDEX_INT_Q_B 8
#define EE_INDEX_EXT_Q_A 4
#define EE_INDEX_EXT_Q_B 9

#define I2C_ADC_ADDR_FIRST 0xD0 // MCP3421 address
#define I2C_ADC_ADDR_LAST 0xDE // MCP3421 address

#define INT_ADC_FILTER_SIZE 64 // recommend power of 2 (2,4,8,16,32,64...)

// use SPRINTF or size optimized code
// SPRINTF variant need to force _sbrk function by "__attribute__ ((noinline, used))" in Core/Src/sysmem.c
// https://mcuoneclipse.com/2017/11/12/solving-problem-with-gnu-linker-and-referenced-in-section-defined-in-discarded-section-error-message/
#define USE_SPRINTF 0


uint16_t adcData[ADC_TABLE_SIZE*2];
static uint16_t secondFltrBuff[ADC_TABLE_SIZE][INT_ADC_FILTER_SIZE];
static int32_t secondFltrSum[ADC_TABLE_SIZE];
static uint32_t secondFltrPos = 0u;

static float intAdcCalibration;

int32_t extAdcVal;
static bool adcNewData = false;
static bool oledUpdate = false;
static bool beeperEnable = true;
static bool systemBeep = false;
static bool powerOffSequence = false;
static uint32_t  systemBeepStop = 0;
static float battV = 0.0f;
static int batt_mV_i = 0;
static int batt_mV_min = 1600;
static int batt_mV_warning = 2000;
static int batt_mV_max = 3200;

static bool forceCalibration = false;

static uint32_t uptime;
static bool adcNewDataSample;

static int32_t extOffset;
static int32_t intOffset;

static float extQestA;
static float extQestB;
static float extRraw;
static float extX10;
static float intQestA;
static float intQestB;
static float intRraw;
static float intX20;

static u8g2_t u8g2;

static uint8_t flipMode;

static uint32_t lastUpdateExtAdc;

static int setupState = 0;

static float intU;

#if 0
  static float lastExtR;
#endif
static float lastIntR;

enum {
  STATE_NORMAL,
  STATE_BOOT,
  STATE_SETUP,
  STATE_OFF
} mainState = STATE_BOOT;

static uint32_t lastActivityUptime;
static uint32_t powerOffBeep = 0;

static uint16_t i2cAdcAddr;

static uint32_t buttonTime[2];
static bool buttonPressed[2];

enum buttonEvent_t  {
    BUTTON_NOTHING,
    BUTTON_SHORT_PRESS_EVENT,
    BUTTON_SHORT_PRESS_ACK,
    BUTTON_LONG_PRESS_EVENT,
    BUTTON_LONG_PRESS_ACK,
};

static enum buttonEvent_t buttonEvent[2] = {BUTTON_NOTHING, BUTTON_NOTHING};

static void enableMainPower(void)
{
  GPIOA -> ODR |= GPIO_PIN_5;
  //HAL_GPIO_WritePin(GPIOA, GPIO_PIN_0, GPIO_PIN_SET);
}

void disableMainPower(void)
{
  GPIOA -> ODR &= ~GPIO_PIN_5;
}

static void saveSettings(void)
{
  uint32_t tmp;
  EE_Status ee_status = EE_ReadVariable32bits(EE_INDEX_FLIPMODE, &tmp); // 8bit only but we have small flash and need only one EEprom read/write variant
  if ((ee_status != EE_OK) || ((uint32_t)flipMode != tmp))
  {
    ee_status = EE_WriteVariable32bits(EE_INDEX_FLIPMODE, (uint32_t)flipMode);
    if(ee_status != EE_OK) {Error_Handler();}
  }

  ee_status = EE_ReadVariable32bits(EE_INDEX_BEEPER, &tmp); // 8bit only but we have small flash and need only one EEprom read/write variant
  if ((ee_status != EE_OK) || ((uint32_t)beeperEnable != tmp))
  {
    ee_status = EE_WriteVariable32bits(EE_INDEX_BEEPER, (uint32_t)beeperEnable);
    if(ee_status != EE_OK) {Error_Handler();}
  }
}

static void enableMeasureCurrent(void)
{
  GPIOA -> ODR |= GPIO_PIN_12;
}

void disableMeasureCurrent(void)
{
  GPIOA -> ODR &= ~GPIO_PIN_12;
}

void shortyPenInit(void)
{
  intAdcCalibration = (3.0f * (float)(*VREFINT_CAL_ADDR)) / 4095.0f;
  HAL_Init();

  /* Unlock the Flash Program Erase controller */
  HAL_FLASH_Unlock();

  // init EEPROM simulation
  (void) EE_Init(EE_FORCED_ERASE);

  EE_Status ee_status = EE_ReadVariable32bits(EE_INDEX_EXT_ZERO, (uint32_t*)&extOffset);
  if(ee_status != EE_OK) forceCalibration = true;
  ee_status = EE_ReadVariable32bits(EE_INDEX_INT_ZERO, (uint32_t*)&intOffset);
  if(ee_status != EE_OK) forceCalibration = true;

  ee_status = EE_ReadVariable32bits(EE_INDEX_EXT_Q_A, (uint32_t*)&extQestA);
  if(ee_status != EE_OK) extQestA = 0.0f;
  ee_status = EE_ReadVariable32bits(EE_INDEX_EXT_Q_B, (uint32_t*)&extQestB);
  if(ee_status != EE_OK) extQestB = 1.0f;
  ee_status = EE_ReadVariable32bits(EE_INDEX_INT_Q_A, (uint32_t*)&intQestA);
  if(ee_status != EE_OK) intQestA = 0.0f;
  ee_status = EE_ReadVariable32bits(EE_INDEX_INT_Q_B, (uint32_t*)&intQestB);
  if(ee_status != EE_OK) intQestB = 1.0f;

  uint32_t tmp;
  ee_status = EE_ReadVariable32bits(EE_INDEX_FLIPMODE, &tmp); // 8bit only but we have small flash and need only one EEprom read/write variant
  flipMode = ((ee_status != EE_OK) || (tmp >= 1)) ? 1 : 0;

  ee_status = EE_ReadVariable32bits(EE_INDEX_BEEPER, &tmp); // 8bit only but we have small flash and need only one EEprom read/write variant
  beeperEnable = ((ee_status != EE_OK) || (tmp >= 1)) ? 1 : 0;


  // timer for beeper
  LL_TIM_EnableCounter(TIM14);
  //LL_TIM_OC_SetCompareCH1(TIM14, 0);
  //LL_TIM_SetAutoReload(TIM14, 65535);
  LL_TIM_CC_EnableChannel(TIM14, LL_TIM_CHANNEL_CH1);

  //HAL_TIM_PWM_Start (&htim14, TIM_CHANNEL_1);

  enableMainPower();

  HAL_ADCEx_Calibration_Start(&hadc1);
  HAL_ADC_Init(&hadc1);
  HAL_ADC_Start_DMA (&hadc1, (uint32_t*)adcData, ADC_TABLE_SIZE*2);

  u8g2_Setup_ssd1306_i2c_128x32_univision_f(&u8g2, U8G2_R0, u8x8_byte_hw_i2c, u8x8_stm32_gpio_and_delay_cb); // init u8g2 structure
  u8g2_InitDisplay(&u8g2); // send init sequence to the display, display is in sleep mode after this,
  u8g2_SetPowerSave(&u8g2, 0); // wake up display
  u8g2_SetFlipMode(&u8g2, flipMode);
  u8g2_SetFont(&u8g2, u8g2_font_fub11_tn);

  // find MCP3421 device
  for (i2cAdcAddr = I2C_ADC_ADDR_FIRST;  i2cAdcAddr <= I2C_ADC_ADDR_LAST; i2cAdcAddr+=2) // break inside
  {
    uint8_t i2cBuf[1];
#if  EXT_ADC_BITS == 18
    i2cBuf[0] = 0x1c;
#elif EXT_ADC_BITS == 16
    i2cBuf[0] = 0x18;
#endif
    HAL_StatusTypeDef i2cRet = HAL_I2C_Master_Transmit(&hi2c1, i2cAdcAddr, i2cBuf, 1, HAL_MAX_DELAY);
    if (i2cRet == HAL_OK) break; // device found
  }

  lastUpdateExtAdc = HAL_GetTick();
}

static void bootScreen(void)
{
  static uint32_t lastBootScreenUpdate = 0u;
  if ((uptime - lastBootScreenUpdate) > 200)
  {
    lastBootScreenUpdate = uptime;
    if (!buttonPressed[BUTTON1_I])
    {
      //mainState = STATE_OFF;
      //disableMeasureCurrent();
      disableMainPower();
    }
    int x = (batt_mV_i + 5) / 10;
    u8g2_ClearBuffer(&u8g2);
#if USE_SPRINTF
    char buf[13];
    sprintf(buf, "%d.%02d", x/100, x%100);
#else
    char buf[5];
    buf[4] = '\0';
    buf[3] = '0' + (char)(x % 10); x /= 10;
    buf[2] = '0' + (char)(x % 10); x /= 10;
    buf[1] = '.';
    buf[0] = '0' + (char)x;
#endif

    // Battery state
    u8g2_DrawXBM(&u8g2,
        0, 31 - SYMBOL_Batt_image_height,
        SYMBOL_Batt_image_width, SYMBOL_Batt_image_height,
        symbol_Batt_image_bits);
    u8g2_uint_t w = u8g2_DrawStr(&u8g2, SYMBOL_Batt_image_width + 4 , 31, buf);
    u8g2_DrawXBM(&u8g2,
        SYMBOL_Batt_image_width + 4 + w, 31 - SYMBOL_V_image_height,
        SYMBOL_V_image_width, SYMBOL_V_image_height,
        symbol_V_image_bits);

    // version of MCP3421
    u8g2_DrawXBM(&u8g2,
        SYMBOL_Batt_image_width + 4 + w + SYMBOL_V_image_width + 16, 31 - SYMBOL_A_image_height,
        SYMBOL_A_image_width, SYMBOL_A_image_height,
        symbol_A_image_bits);
    if (i2cAdcAddr <= I2C_ADC_ADDR_LAST)
    {
      buf[0] = '0' + (char)((i2cAdcAddr - I2C_ADC_ADDR_FIRST) / 2);
      buf[1] = '\0';
      u8g2_DrawStr(&u8g2, SYMBOL_Batt_image_width + 4 + w + SYMBOL_V_image_width + 16 + SYMBOL_A_image_width, 31, buf);
    }

    // version
    u8g2_DrawXBM(&u8g2,
        0, 11 - SYMBOL_V_image_height,
        SYMBOL_V_image_width, SYMBOL_V_image_height,
        symbol_V_image_bits);
    u8g2_DrawStr(&u8g2, SYMBOL_V_image_width + 2 , 11, SW_VERSION);
    oledUpdate = true;
  }
}

static void bigRscreen()
{
  u8g2_ClearBuffer(&u8g2);
  u8g2_DrawXBM(&u8g2,
      0, 24 - 4  - SYMBOL_more_image_height,
      SYMBOL_more_image_width, SYMBOL_more_image_height,
      symbol_more_image_bits);
  u8g2_uint_t w = u8g2_DrawStr(&u8g2, 12, 24, "10");
  u8g2_DrawXBM(&u8g2,
      12 + w + 4, 24 - SYMBOL_omega_image_height,
      SYMBOL_omega_image_width, SYMBOL_omega_image_height,
      symbol_omega_image_bits);
  oledUpdate = true;
}

static void rScreen35(int ohm_10000, float factor)
{
  u8g2_ClearBuffer(&u8g2);
#if USE_SPRINTF
  char buf[14];
  sprintf(buf, "%d.%04d", ohm_10000/10000, ohm_10000%10000);
#else
  char buf[7];
  buf[6] = '\0';
  buf[5] = '0' + (char)(ohm_10000 % 10);
  int x = ohm_10000/10;
  buf[4] = '0' + (char)(x % 10); x /= 10;
  buf[3] = '0' + (char)(x % 10); x /= 10;
  buf[2] = '0' + (char)(x % 10); x /= 10;
  buf[1] = '.';
  buf[0] = '0' + (char)x;
#endif
  u8g2_DrawStr(&u8g2, 0, 24, buf);
  u8g2_uint_t w = u8g2_DrawStr(&u8g2, 0, 24, buf);
  u8g2_DrawXBM(&u8g2,
      w + 4, 24 - SYMBOL_omega_image_height,
      SYMBOL_omega_image_width, SYMBOL_omega_image_height,
      symbol_omega_image_bits);

  u8g2_DrawBox(&u8g2, 0, 27, (u8g2_uint_t)(factor*128.0f) , 5);
  oledUpdate = true;
}

static void rScreen10(int ohm_100, float factor)
{
  u8g2_ClearBuffer(&u8g2);
#if USE_SPRINTF
  char buf[14];
  sprintf(buf, "%d.%03d", ohm_100/100, ohm_100%100);
#else
  char buf[5];
  buf[4] = '\0';
  buf[3] = '0' + (char)(ohm_100 % 10);
  int x = ohm_100/10;
  buf[2] = '0' + (char)(x % 10); x /= 10;
  buf[1] = '.';
  buf[0] = '0' + (char)x;
#endif
  u8g2_DrawStr(&u8g2, 0, 24, buf);
  u8g2_uint_t w = u8g2_DrawStr(&u8g2, 0, 24, buf);
  u8g2_DrawXBM(&u8g2,
      w + 4, 24 - SYMBOL_omega_image_height,
      SYMBOL_omega_image_width, SYMBOL_omega_image_height,
      symbol_omega_image_bits);

  u8g2_DrawBox(&u8g2, 0, 27, (u8g2_uint_t)(factor*128.0f) , 5);
  oledUpdate = true;
}


static void commonScreen()
{
  if (oledUpdate)
  {

    // battery status
    int battStatus;
    {
      battStatus = ((batt_mV_i - batt_mV_min) * 17) / (batt_mV_max - batt_mV_min);
      if (battStatus > 17)
      {
        battStatus = 17;
      }
      else if (battStatus < 0)
      {
        battStatus = 0;
      }
    }
    if ((batt_mV_i > batt_mV_warning) || ((uptime & 512) != 0)) // indicator blink
    {
      u8g2_DrawXBM(&u8g2,
          117, 0,
          SYMBOL_BattBar_image_width, SYMBOL_BattBar_image_height,
          symbol_BattBar_image_bits);
      u8g2_DrawBox(&u8g2, 119, 22 - battStatus , 6, battStatus);   // battery status
    }

    // speaker symbol
    u8g2_DrawXBM(&u8g2,
        117 -4 -8 -2 -8, 0,
        SYMBOL_speakerA_image_width, SYMBOL_speakerA_image_height,
        symbol_speakerA_image_bits);
    if (beeperEnable)
    {
      u8g2_DrawXBM(&u8g2,
          117 -4 -8, 0,
          SYMBOL_speakerB_image_width, SYMBOL_speakerB_image_height,
          symbol_speakerB_image_bits);
    }

    u8g2_SendBuffer(&u8g2);

    oledUpdate = false;
  }
}

static void beeperOff(void)
{
  LL_TIM_OC_SetCompareCH1(TIM14, 0);
}

static void beeperOn(uint16_t arrI)
{
  LL_TIM_OC_SetCompareCH1(TIM14, ARR_2000/8); // constant open is enough, we don't need 50%
  LL_TIM_SetAutoReload(TIM14, arrI);
}

static void externalADCread (void)
{
  lastUpdateExtAdc = uptime;
  uint8_t i2cBuf[EXT_ADC_BUFFER];
  HAL_StatusTypeDef ret = HAL_I2C_Master_Receive(&hi2c1, i2cAdcAddr, i2cBuf, EXT_ADC_BUFFER, HAL_MAX_DELAY);
  if ( ret == HAL_OK )
  {
    if ((i2cBuf[EXT_ADC_BUFFER - 1] & 0x80u) == 0) // RDY bit
    {
#if  EXT_ADC_BITS == 18
      extAdcVal = (int)(
                  (((uint32_t)i2cBuf[0]) << 16) |
                  (((uint32_t)i2cBuf[1]) << 8) |
                  ((uint32_t)i2cBuf[2]));
#elif  EXT_ADC_BITS == 16
      extAdcVal = (int)(
                  (((uint32_t)i2cBuf[0]) << 8) |
                  ((uint32_t)i2cBuf[1]));
#endif
    }
  }
}

static void systemBeepStart(uint16_t arrI, uint32_t duration)
{
  systemBeep = true;
  systemBeepStop = uptime + duration;
  beeperOn(arrI);
}

static void updateLastActivity(void)
{
  lastActivityUptime = uptime;
  powerOffBeep = 0;
}

static void buttonStates (int n, uint16_t val, uint16_t thld, uint16_t hysteresis )
{
  bool pressed;
  if (buttonPressed[n])
  {
    pressed = (val >= (thld-hysteresis));
  }
  else
  {
    pressed = (val >= (thld+hysteresis));
  }

  if (pressed)
  {
    if (buttonPressed[n] == false)
    {
      buttonTime[n] = uptime;
      buttonPressed[n] = true;
      updateLastActivity();
    }
    else
    {
      // long press detector
      if ((buttonEvent[n] != BUTTON_LONG_PRESS_ACK) && ((uptime - buttonTime[n]) >= BUTTON_LONG_TIME))
      {
        buttonEvent[n] = BUTTON_LONG_PRESS_EVENT;
      } else {}
    }
  }
  else
  {
    if (buttonPressed[n])
    {
      buttonPressed[n] = false;
      if ((buttonEvent[n] == BUTTON_NOTHING) && ((uptime - buttonTime[n]) >= BUTTON_SHORT_TIME))
      {
        buttonEvent[n] = BUTTON_SHORT_PRESS_EVENT;
      } else {}
      updateLastActivity();
    }
    else
    {
      if ((buttonEvent[n] == BUTTON_SHORT_PRESS_ACK) || (buttonEvent[n] == BUTTON_LONG_PRESS_ACK))
      {
        buttonEvent[n] = BUTTON_NOTHING;
      }
      else {}
    }
  }
}

static void bootState_func(void)
{
  if (uptime < 2000)
  {
    bootScreen();
    if (uptime > 1600)
    {
      enableMeasureCurrent();
    }
  }
  else
  {
    systemBeepStart(ARR_1760, 100);
    buttonEvent[BUTTON1_I] = BUTTON_LONG_PRESS_ACK; // suppress false button event from power-on button
    if ((buttonEvent[BUTTON2_I] == BUTTON_LONG_PRESS_EVENT) || forceCalibration) // goto setup screen
    {
      buttonEvent[BUTTON2_I] = BUTTON_LONG_PRESS_ACK;
      mainState = STATE_SETUP;
    }
    else
    {
      mainState = STATE_NORMAL;
    }
    u8g2_SetFont(&u8g2, u8g2_font_fub20_tn);
  }
}

static void commonButtonActions(void)
{
  if (buttonEvent[BUTTON1_I] == BUTTON_LONG_PRESS_EVENT)
  {
    saveSettings();
    systemBeepStart(ARR_1760, 200);
    powerOffSequence = true;
    buttonEvent[BUTTON1_I] = BUTTON_LONG_PRESS_ACK;
  }
}

static void normalButtonActions(void)
{
  // SW2 short press enable/disable beeper indication
  if (buttonEvent[BUTTON2_I] == BUTTON_SHORT_PRESS_EVENT)
  {
    beeperEnable = !beeperEnable;
    buttonEvent[BUTTON2_I] = BUTTON_SHORT_PRESS_ACK;
  }

  // SW2 long press rotate display
  if (buttonEvent[BUTTON2_I] == BUTTON_LONG_PRESS_EVENT)
  {
    flipMode = (flipMode != 0) ? 0 : 1;

    u8g2_SetFlipMode(&u8g2, flipMode);
    buttonEvent[BUTTON2_I] = BUTTON_LONG_PRESS_ACK;
  }
}

static void setupState_func(float extR)
{
  u8g2_ClearBuffer(&u8g2);
  // "Cal" symbol
  u8g2_DrawXBM(&u8g2,
      0, 24 - SYMBOL_Cal_image_height,
      SYMBOL_Cal_image_width, SYMBOL_Cal_image_height,
      symbol_Cal_image_bits);

  char buf[4];
  buf[0] = '0' + (char)(setupState/10); // "0.0", "1.0", "2.0" and "8.2" ohm calibration
  buf[1] = '.';
  buf[2] = '0' + (char)(setupState%10);
  buf[3] = '\0';
  u8g2_uint_t w = u8g2_DrawStr(&u8g2, SYMBOL_Cal_image_width + 4, 24, buf);
  // "omega" symbol
  u8g2_DrawXBM(&u8g2,
      SYMBOL_Cal_image_width + 4 + 4 + w, 24 - SYMBOL_omega_image_height,
      SYMBOL_omega_image_width, SYMBOL_omega_image_height,
      symbol_omega_image_bits);

  if (buttonEvent[BUTTON2_I] == BUTTON_SHORT_PRESS_EVENT)
  {
    switch (setupState)
    {
      case 0:
        extOffset = extAdcVal;
        intOffset = secondFltrSum[ADC_CH_SENSOR];
        systemBeepStart(ARR_1760, 50);
        setupState = 10;
        break;
      case 10:
        extX10 = extRraw; // first point for external ADC
        systemBeepStart(ARR_1760, 50);
        setupState = 20;
        break;
      case 20:
      {
        float extX20 = extRraw; // second point for external ADC
        // external ADC constants for quadratic estimation (points 1.0 and 2.0)
        extQestA = (2.0f - 1.0f*extX20/extX10)/(extX20*extX20 - extX10*extX20);
        extQestB = 1.0f/extX10 - extQestA*extX10;

        intX20 = intRraw; // first point for internal ADC
        systemBeepStart(ARR_1760, 50);
        setupState = 82;
        break;
      }
      case 82:
      default:
      {
        // internal ADC constants for quadratic estimation (points 2.0 and 8.2)
        float intX82 = intRraw; // second point for internal ADC
        intQestA = (8.2f - 2.0f*intX82/intX20)/(intX82*intX82 - intX20*intX82);
        intQestB = 2.0f/intX20 - intQestA*intX20;

        // save values for estimation
        EE_Status ee_status = EE_WriteVariable32bits(EE_INDEX_EXT_ZERO, (uint32_t)extOffset);
        if(ee_status != EE_OK) {Error_Handler();}
        uint32_t* p32 = (uint32_t*)&extQestA;
        ee_status = EE_WriteVariable32bits(EE_INDEX_EXT_Q_A, *p32);
        if(ee_status != EE_OK) {Error_Handler();}
        p32 = (uint32_t*)&extQestB;
        ee_status = EE_WriteVariable32bits(EE_INDEX_EXT_Q_B, *p32);
        if(ee_status != EE_OK) {Error_Handler();}
        ee_status = EE_WriteVariable32bits(EE_INDEX_INT_ZERO, (uint32_t)intOffset);
        if(ee_status != EE_OK) {Error_Handler();}
        p32 = (uint32_t*)&intQestA;
        ee_status = EE_WriteVariable32bits(EE_INDEX_INT_Q_A, *p32);
        if(ee_status != EE_OK) {Error_Handler();}
        p32 = (uint32_t*)&intQestB;
        ee_status = EE_WriteVariable32bits(EE_INDEX_INT_Q_B, *p32);
        if(ee_status != EE_OK) {Error_Handler();}

        setupState = 0;
        systemBeepStart(ARR_1760, 100);
        mainState = STATE_NORMAL;
        break;
      }
    }
    buttonEvent[BUTTON2_I] = BUTTON_SHORT_PRESS_ACK;
  }

  oledUpdate = true;
}

static void normalState_func(float extR, float intR)
{
  int ohm_10000 = lroundf(extR * 10000);
  float factor;
  // calculate factor
  if (ohm_10000 < 10000)
  {
    // note https://stackoverflow.com/questions/65986056/is-there-a-non-looping-unsigned-32-bit-integer-square-root-function-c
    factor = sqrtf(extR); //smaller than powf(extR, 1.0f/4.0f);
  }
  else
  {
    factor = 1.0f;
  }
  // beeper
  if (!systemBeep)
  {
    if (beeperEnable && (factor < 1.0f))
    {
      float arr = ((float) ARR_2000 + (factor * (((float) ARR_400 - (float) ARR_2000))));
      uint16_t arrI = (uint16_t) arr;
      beeperOn(arrI);
    }
    else
    {
      beeperOff();
    }
  } else {}
  // screen
  if (ohm_10000 > (35000))
  {
    int ohm_100int = lroundf(intR * 100);
    if (ohm_100int < 1000)
    {
      rScreen10(ohm_100int, factor);
    }
    else
    {
      bigRscreen();
    }
  }
  else
  {
    rScreen35(ohm_10000, factor);
  }
}

static void autoPowerOffCheck(void)
{
  if ((uptime - lastActivityUptime) > INACTIVE_POWER_OFF_TIME)
  {
    if ((uptime - lastActivityUptime) > (INACTIVE_POWER_OFF_TIME + powerOffBeep))
    {
      if (powerOffBeep >= (3 * INACTIVE_POWER_OFF_TIME_BEEPER))
      {
        if (powerOffBeep >= (3 * INACTIVE_POWER_OFF_TIME_BEEPER + 1000))
        {
          saveSettings();
          mainState = STATE_OFF;
          disableMeasureCurrent();
          disableMainPower();
        }
        else
        {
          powerOffBeep += 1000;
          systemBeepStart(ARR_1760, 1000);
        }
      }
      else
      {
        systemBeepStart(ARR_1760, 100);
        powerOffBeep += INACTIVE_POWER_OFF_TIME_BEEPER;
      }
    }
  }
}

static void powerOffScreen(void)
{
  static bool active = false;
  if (active == false)
  {
    u8g2_ClearBuffer(&u8g2);
    // "Off" symbol
    u8g2_DrawXBM(&u8g2,
        (128-SYMBOL_Off_image_width)/2, (32 - SYMBOL_Off_image_height) / 2,
        SYMBOL_Off_image_width, SYMBOL_Off_image_height,
        symbol_Off_image_bits);
    u8g2_SendBuffer(&u8g2);
    active = true;
  }
}

void shortyPenLoop(void)
{
  static float intR;

  uptime = HAL_GetTick();
  uint32_t diff1 = uptime - lastUpdateExtAdc;
  adcNewDataSample = adcNewData;
  if (diff1 > 2)
  {
    externalADCread();
  }
  if (adcNewDataSample)
  {
    float conv = intAdcCalibration / (float)secondFltrSum[ADC_CH_Vref];
    battV = (float)(secondFltrSum[ADC_CH_BATT]) * conv;
    //battV *= 1.035f; // R26=1M current loss compensation, tricky but enough for battery indicator
    battV *= 1.007f; // R26=10k current loss compensation, tricky but enough for battery indicator
    batt_mV_i = lroundf(battV * 1000.0f);

    buttonStates(BUTTON1_I, adcData[ADC_CH_SW1], ADC_SW1_THLD, ADC_SW1_HYST);
    buttonStates(BUTTON2_I, adcData[ADC_CH_SW2], ADC_SW2_THLD, ADC_SW2_HYST);

//    static float intU;
    intU = ((float)(secondFltrSum[ADC_CH_SENSOR]-intOffset)) * (conv / GAIN);
    intRraw = CONST_E / (1.0f/intU - CONST_F);
    intR = intRraw * (intQestA*intRraw + intQestB); // quadratic estimation
    if (intR < 0.0f) intR = 0.0f;
    if ((intR < 10.0f) && (fabs(lastIntR - intR) > 1.0f))
    {
      updateLastActivity();
      lastIntR = intR;
    }

    adcNewData = false;
  }



  if (systemBeep)
  {
    if ((int32_t) (uptime - systemBeepStop) >= 0) // conversion to signed to use mod32 space
    {
      systemBeep = false;
      beeperOff();
      if (powerOffSequence)
      {
        mainState = STATE_OFF;
        disableMeasureCurrent();
        disableMainPower();
        powerOffSequence = false;
      }
    } else {}
  }

  autoPowerOffCheck();

  float extU = ((float)(extAdcVal-extOffset)) / (16000.0f * GAIN);
  float extR;
  extRraw = CONST_E / (1.0f/extU - CONST_F);
  extR = extRraw * (extQestA*extRraw + extQestB);
  if (extR < 0.0f) extR = 0.0f;
#if 0
  if (fabs(lastExtR - extR) > 1.0f)
  {
    updateLastActivity();
    lastExtR = extR;
  }
#endif

  switch (mainState)
  {
    case STATE_NORMAL:
    default:
      commonButtonActions();
      normalButtonActions();
      normalState_func(extR, intR);
      break;
    case STATE_BOOT:
      bootState_func();
      break;
    case STATE_SETUP:
      commonButtonActions();
      setupState_func(extR);
      break;
    case STATE_OFF:
      powerOffScreen();
      break;
  }
  commonScreen();
}


// Called when first half of buffer is filled
void HAL_ADC_ConvHalfCpltCallback(ADC_HandleTypeDef* hadc) {
  secondFltrPos ++;
  secondFltrPos %= INT_ADC_FILTER_SIZE;
  for (uint32_t i=0u; i < ADC_TABLE_SIZE; i++)
  {
    secondFltrSum[i] -= (int32_t)secondFltrBuff[i][secondFltrPos];
    secondFltrSum[i] += (int32_t)adcData[i];
    secondFltrBuff[i][secondFltrPos] = adcData[i];
  }
  adcNewData = true;
}

// Called when buffer is completely filled
void HAL_ADC_ConvCpltCallback(ADC_HandleTypeDef* hadc)
{
  secondFltrPos ++;
  secondFltrPos %= INT_ADC_FILTER_SIZE;
  for (uint32_t i=0u; i < ADC_TABLE_SIZE; i++)
  {
    secondFltrSum[i] -= (int32_t)secondFltrBuff[i][secondFltrPos];
    secondFltrSum[i] += (int32_t)adcData[i + ADC_TABLE_SIZE];
    secondFltrBuff[i][secondFltrPos] = adcData[i + ADC_TABLE_SIZE];
  }
  adcNewData = true;
}

