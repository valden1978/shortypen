/*
 * adc.h
 *
 *  Created on: Jul 13, 2022
 *      Author: jiri.dobry
 */

#ifndef SRC_ADC_H_
#define SRC_ADC_H_


#define ADC_CH_SENSOR 0
#define ADC_CH_SW1 1
#define ADC_CH_SW2 2
#define ADC_CH_BATT 3
#define ADC_CH_TEMP 4
#define ADC_CH_Vref 5

#define ADC_TABLE_SIZE (ADC_CH_Vref+1)


/*
 * treshold value for buttons
 *
 * 10000 is ~ 0.504V
 * 30000 is ~ 1.512V
 */
#define ADC_SW1_THLD 10000
#define ADC_SW1_HYST 1000
#define ADC_SW2_THLD 30000
#define ADC_SW2_HYST 3000


//#define EXT_ADC_BITS 18
#define EXT_ADC_BITS 16

#if  EXT_ADC_BITS == 18
#  define EXT_ADC_BUFFER 4
#elif  EXT_ADC_BITS == 16
#  define EXT_ADC_BUFFER 3
#else
#  error invalid EXT_ADC_BITS
#endif


#define RESISTOR_R6 (30.0f)
#define RESISTOR_R7 (1.0f)
#define RESISTOR_R8 (1.0f)
#define RESISTOR_R9 (1.0f)
#define MEASURE_U (1.85f)
#define GAIN (27.0f)

#define RESISTOR_R7_R8 (RESISTOR_R7 + RESISTOR_R8)

#define CONST_E ((RESISTOR_R6 + RESISTOR_R9) / MEASURE_U)
#define CONST_F ((RESISTOR_R6 + RESISTOR_R7_R8 + RESISTOR_R9) / (MEASURE_U * RESISTOR_R7_R8))

#endif /* SRC_ADC_H_ */
