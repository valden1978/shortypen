# shortyPen

- [manufacturing bill of materials](BOM.md)
- [Datasheet links](link.md)
- [Released + precompiled ELF files](SW_release)
- [Model for 3D print](3D_print)

## Original idea links

- https://www.eevblog.com/forum/testgear/finding-short-on-motherboards-with-a-shorty-(with-display)/
- https://hackaday.io/project/3635-shorty-short-circuit-finder
- http://kripton2035.free.fr/Projects/proj-shorty.html

## Demonstration video of 1st prototype

https://youtu.be/PTSSFycWCzc

## Schematics and PCB design
[shortyPen schematics and PCB](https://oshwlab.com/jdobry/shortypen)

[Calibration and demonstration board for shortyPen](https://oshwlab.com/jdobry/r-test)

## High-resolution images
https://photos.app.goo.gl/9pkDtEAfapqZDasC6

## Programming clip
[probeClothespeg-001](https://www.thingiverse.com/thing:3093212)

## Compilation from sources
Clone it from gitlab repository in "recursive" mode.
```
git clone --recursive "git@gitlab.com:jdobry/shortypen.git"
```
OR
```
git clone --recursive https://gitlab.com/jdobry/shortypen.git
```
You need u8g2 library as submodule. If you forget to use "recursive" mode, you can add it to project later

```
git submodule init
git submodule update
```

For compilation use [STM32CubeIDE](https://www.st.com/en/development-tools/stm32cubeide.html) 

## License
Copyright (c) 2023 Jiri Dobry

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

## Project status
If you have run out of energy or time for your project, put a note at the top of the README saying that development has slowed down or stopped completely. Someone may choose to fork your project or volunteer to step in as a maintainer or owner, allowing your project to keep going. You can also make an explicit request for maintainers.
