| Name | Designator |Footprint|
| ------ | ------ | ------- |
|0.1u|C1,C4,C6,C8,C9,C11,C12,C14,C15,C16,C18|C0805|
|10u|C2,C3,C5,C7,C10,C13,C19|C1206|
|10p|C17|C0603|
|BAS40-05-TP|D2|SOT-23-3|
|1N4148WS T4|D3|SOD-323|
|470nH|L1|IND-SMD_L4.3-W4.0_GPSR0420|
|6.3nH|L2|L0603|
|[CMT-7525-80-SMT-TR](https://www.mouser.com/ProductDetail/cui-devices/cmt-7525-80-smt-tr/?qs=P1JMDcb91o5izP7Xira9ww%3d%3d)|LS1|XDCR_CMT-7525-80-SMT-TR|
|BC817-16|Q1|SOT-23-3|
|1k|R1,R2,R13,R18,R19,R25|R0603|
|27k|R3,R4|R0603|
|100k|R5,R15,R16,R22|R0603|
|30R|R6|R1206|
|1R|R7,R8|R2512|
|1R|R9|R1206|
|10k|R10,R11,R12,R20,R26|R0603|
|1M|R14,R21|R0603|
|0R|R17|R0603|
|316k|R23|R0603|
|[EVPBFAC1A000](https://cz.mouser.com/ProductDetail/Panasonic/EVP-BFAC1A000?qs=OiaALIbqWFSeImjptaxnOQ%3D%3D)|SW1,SW2|KEY-SMD_4P-L6.0-W6.0-P4.00-LS6.6|
|[TPS7A20185PDBVR](https://cz.mouser.com/ProductDetail/Texas-Instruments/TPS7A20185PDBVR?qs=hd1VzrDQEGgQiYm%252BUIe6ow%3D%3D)|U1|SOT-23-5|
|[TPS61021ADSGR](https://cz.mouser.com/ProductDetail/Texas-Instruments/TPS61021ADSGR?qs=osPbIpHqQ9VA%2FzqN%2FQBaxg%3D%3D)|U2|WSON-8|
|[AD8628WAUJZ-RL](https://cz.mouser.com/ProductDetail/Analog-Devices/AD8628WAUJZ-R7?qs=%2FtpEQrCGXCxWMM9t6I0jCw%3D%3D)|U3|TSOT-5|
|[MCP3421A3T-E/CH](https://cz.mouser.com/ProductDetail/Microchip-Technology-Atmel/MCP3421A3T-E-CH?qs=hH%252BOa0VZEiCiOakdR18pNw%3D%3D) or any MCP3421Ax|U4|SOT-23-6|
|[STM32G030F6P6](https://cz.mouser.com/ProductDetail/STMicroelectronics/STM32G030F6P6?qs=uwxL4vQweFOQRcVHghCbHA%3D%3D)|U5|TSSOP-20|
|[OLED 0.91 Inch](https://www.aliexpress.com/item/32927682460.html)|U6|OLED 0.91 INCH|
|[Needles Pin with no thread](https://s.click.aliexpress.com/e/_DBPslrD)
|[AAA Battery Spring Positive And Negative Contact](https://www.aliexpress.com/item/33053754850.html)
